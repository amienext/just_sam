# Just SAM

## A standalone(ish) version of the SuperDARN Assimilative Mapping

### What is SAM?

SAM is a data assimilation procedure inspired by Art Richmond's (NCAR HAO) Assimilative Mapping of Ionospheric Electrodynamics
that assimilates line of sight ion velocities from the SuperDARN Coherent Scatter Radar system and 
predicts electrodynamic quanitites such as electric field and potential globally, using a variation on
optimal interpolation and the emperical CS10 model to best estimate values beyond the coverage of the LOS data
ingested. The procedure was created by Ellen D. P. Cousins and Tomoko Matsuo.

The procedure is described in detail in a series of papers:

Cousins, E. D. P., T. Matsuo, and A. D. Richmond (2013), SuperDARN assimilative mapping, J. Geophys. Res. Space Physics, 118, 7954–7962, doi:10.1002/2013JA019321.

Cousins, E. D. P., T. Matsuo, and A. D. Richmond (2013), Mesoscale and large-scale variability in high-latitude ionospheric convection: Dominant modes and spatial/temporal coherence, J. Geophys. Res. Space Physics, 118, 7895–7904, doi:10.1002/2013JA019319.

### What is this project?

Just SAM is the first effort of the AMIENext Python project, which aims to produce professional quality
python implementations of the next generation of estimation procedures based on the Assimilative Mapping of Ionospheric Electrodynamics.

### Installation

You will need VT SuperDARN's DavitPy software installed before you can use SAM.

Go [here](https://github.com/vtsuperdarn/davitpy) to get it, and follow their installation instructions **carefully**. 
Due to the large number of Fortran and C wrapped packages getting it to install can be difficult. Fortunately,
SAM does not require any of the Fortran models. The only wrapped package it uses is the C languange AACGM package.

Downloaded and install the geospacepy pure python plotting utilities
```{bash}
git clone https://bitbucket.org/amienext/geospacepy-lite
cd geospacepy-lite
python setup.py install
```

To install Just SAM on linux or MacOSX open a terminal and issue the following:
```
git clone https://bitbucket.org/amienext/just_sam
cd just_sam
python setup.py install
```

To test SAM, first start an interactive python or ipython/jupyter shell by issuing
```{bash}
python
```
or
 
```{bash}
ipython
```

at the command line. Then:
```{python}
import just_sam
just_sam.test_sam()
```