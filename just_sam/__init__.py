"""
*********************
**Module**: just_sam
   :synopsis: The SuperDARN Assimilative Mapping procedure as a standalone package
*********************
"""
import matplotlib as mpl
import sam,cs10,basis

def plot_sam_frame(thissam,hemi,f=None,maxpot=30000.):
	import datetime,sys
	import numpy as np
	import matplotlib.pyplot as pp
	from geospacepy import satplottools # To make it easier to visualize

	hemisign = 1. if hemi is 'north' else -1.
	hemiabrev = 'N' if hemi in ['north','N'] else 'S'
	
	if f is None:
		f = pp.figure(figsize=(12,6))
	
	norm = mpl.colors.Normalize(vmin=-1*maxpot,vmax=1.*maxpot)

	#Grid settings
	dlat,dlon,lat0 = 2.,10.,50.
	(flatlat,flatlon,vel_th,vel_ph,verr_th,verr_ph) = thissam.calcFitVel(dlat=dlat,dlon=dlon,lat0=lat0)
	#(lat_grd,lon_grd,el_th,el_ph,eerr_th,eerr_ph)   = samList[0].calcFitEfield(dlat=2,dlon=10,lat0=50)
	(flatlat,flatlon,pot,perr)                      = thissam.calcFitPot(dlat=dlat,dlon=dlon,lat0=lat0)
	obs_lat,obs_lon = np.array(thissam.obs['lat']),np.array(thissam.obs['lon'])
	obs_verr,obs_vlos,obs_azm = np.array(thissam.obs['verr']),np.array(thissam.obs['vlos']),np.array(thissam.obs['azm'])
	obs_vel_th = -1*np.cos(np.radians(obs_azm))*obs_vlos #Equatorward component of line-of-sight directions
	obs_vel_ph = np.sin(np.radians(obs_azm))*obs_vlos #Eastward component of line-of-sight direction
	obs_res = thissam.obs_res.flatten()*1000.
	obs_res_med = np.nanmedian(obs_res)
	obs_res_lq,obs_res_uq = np.nanpercentile(obs_res,25),np.nanpercentile(obs_res,75)

	#Observation relative error
	obs_rerr = (np.abs(obs_vlos)-obs_verr)/np.abs(obs_vlos)
	obs_rerr[obs_rerr>1]=1.
	obs_rerr[obs_rerr<0]=0.
	
	#Get magnetic local time
	#This longitude is actually MLT longitude, i.e MLT in degrees. Very strange
	flatmlt = flatlon/360.*24. 
	#I think this one is real magnetic longitude?
	obs_mlt = np.mod(obs_lon+thissam.mltDef,360)/360.*24.

	lats = np.arange(lat0,90.+dlat,dlat)
	lons = np.arange(-180.,180.,dlon)

	f.clf()
	acb = f.add_axes([.05,.05,.03,.8])
	a = f.add_axes([.15,.05,.35,.8])
	a2 = f.add_axes([.5,.05,.35,.8])
	acb2 = f.add_axes([.85,.05,.03,.8])
	
	satplottools.draw_dialplot(a)
	satplottools.draw_dialplot(a2)

	X,Y = satplottools.latlt2cart(flatlat,flatmlt,hemiabrev)
	Xobs,Yobs = satplottools.latlt2cart(obs_lat,obs_mlt,hemiabrev)

	#Calculate arrow directions for vector plot
	phi = np.mod(obs_mlt-6.,24.)/12.*np.pi

	vel_east = obs_vel_ph
	vel_eq = obs_vel_th

	eq_hat_x,eq_hat_y = np.cos(phi),np.sin(phi)
	east_hat_x,east_hat_y = -1.*np.sin(phi),np.cos(phi)
	
	#Convert vectors to cartesian from top-down polar
	obs_vel_x = vel_east*east_hat_x + vel_eq*eq_hat_x
	obs_vel_y = vel_east*east_hat_y + vel_eq*eq_hat_y
	
	#vel_x = -1*vel_x
	#vel_y = -1*vel_y

	#Make them into grids
	lat_grd = np.reshape(flatlat,(len(lons),len(lats)))
	lon_grd = np.reshape(flatlon,(len(lons),len(lats)))
	
	X_grd, Y_grd = np.reshape(X,(len(lons),len(lats))),np.reshape(Y,(len(lons),len(lats)))
	pot_grd,pot_err_grd = np.reshape(pot,(len(lons),len(lats))),np.reshape(perr,(len(lons),len(lats)))

	#Draw the potential
	#maxpot = np.nanmax(np.abs(pot))
	maxerr = np.nanmax(np.abs(perr))

	cmap_pot,cmap_err,cmap_obs = pp.cm.get_cmap('jet'),pp.cm.get_cmap('jet'),pp.cm.get_cmap('bone')

	levels = np.linspace(-1*maxpot,1*maxpot,20)
	mappable = a.contourf(X_grd,Y_grd,pot_grd,30,cmap=cmap_pot,alpha=.7,vmin=-1.*maxpot,vmax=1.*maxpot)
	
	err_mappable = a2.contourf(X_grd,Y_grd,pot_err_grd,30,cmap=cmap_err,alpha=.7,vmin=0.,vmax=maxerr)
	
	cb = f.colorbar(mappable,label='Potential',cax=acb)	
	cb = f.colorbar(err_mappable,label='Analysis Error in Potential',cax=acb2)

	a.scatter(Xobs[obs_rerr>0.],Yobs[obs_rerr>0.],s=5,c='w',marker='o',edgecolor='none',cmap=cmap_obs,alpha=.6)
	a.scatter(Xobs[obs_rerr==0.],Yobs[obs_rerr==0.],s=5,c='r',marker='o',edgecolor='none',cmap=cmap_obs,alpha=.6)

	a2.scatter(Xobs,Yobs,s=10*obs_rerr+1,c='k',marker='o',edgecolor='none',cmap=cmap_obs)
	a2.scatter(Xobs[obs_rerr==0.],Yobs[obs_rerr==0.],s=5,c='r',marker='o',edgecolor='none',cmap=cmap_obs)

	Q1 = a.quiver(Xobs[obs_rerr>0.], Yobs[obs_rerr>0.], obs_vel_x[obs_rerr>0.], obs_vel_y[obs_rerr>0.], color='black', 
		angles='xy', scale_units='xy', scale=1000./10.,alpha=.6, headwidth=2., headlength=1.,width=.0025)
	a.quiverkey(Q1,.75,0.05,1000.,'SuperDARN Line-of-sight V\n 1000 m/s')
	
	f.suptitle('SuperDARN Assimilative Mapping: %s-%s' % (thissam.sTime.strftime('%c'),thissam.eTime.strftime('%c')))

	a.set_title('Potential and Ion Drift Velocity')
	a2.set_title('Analysis Error')

	f.text(.5,0.02,'Vsw: %.3f, IMF By : %.3f, IMF Bz: %.3f' % (thissam.Vsw,thissam.IMFBy,thissam.IMFBz),ha='center')
	f.text(.5,0.98,'E_los Res [mV/m]: %.3f,%.3f,%.3f' % (obs_res_lq,obs_res_med,obs_res_uq),ha='center')
	return f
	

def test_sam():
	"""
	This is the example in the sam.py docstring implemented as very rudimentary unit test
	The expected values (i.e. the ones we are checking against ) have been confirmed 
	by several versions of the SAM procedure as run by 
	Minjie Fan (UC Davis / NCAR ) and by
	Xueling Shi (VT / SuperDARN lab) 
	and finally by myself using the old version of SAM 

	"""

	import datetime as dt
	import numpy as np
	import matplotlib.pyplot as pp
	            
	samList = sam.calcSam(dt.datetime(2011,1,1,12,0),dt.datetime(2011,1,1,12,56),2)

	#(lat_grd,lon_grd,vel_th,vel_ph,verr_th,verr_ph) = samList[0].calcFitVel(dlat=2,dlon=10,lat0=50)
	#(lat_grd,lon_grd,el_th,el_ph,eerr_th,eerr_ph)   = samList[0].calcFitEfield(dlat=2,dlon=10,lat0=50)
	#(lat_grd,lon_grd,pot,perr)                      = samList[0].calcFitPot(dlat=2,dlon=10,lat0=50)
	
	#Calculate electric field along the line of 70 degrees latitude        
	lat = np.ones(24)*70.
	lon = np.linspace(0,345,24)
	(lat,lon,el_th,el_ph,eerr_th,eerr_ph)   = samList[0].calcFitEfield(lat=lat,lon=lon)

	#These values are from the email chain
	expected_elth = np.array([ 0.00831211,  0.0075146 ,  0.00907228,  0.00536773, -0.00219824,
       -0.00343127,  0.00169708,  0.00382742,  0.00226107,  0.00160865,
        0.00132634, -0.00016353, -0.00333272, -0.00633707, -0.00954862,
       -0.01439453, -0.01741932, -0.01692615, -0.01351956, -0.00751253,
       -0.00177238,  0.00249753,  0.00712057,  0.00995876])

	expected_elph = np.array([ -7.76241675e-03,  -4.25259417e-03,   1.43429115e-03,
         5.82288078e-03,   5.52307158e-03,   2.58892392e-03,
         1.30000337e-03,   2.13807572e-03,   2.48541966e-03,
         1.48648914e-03,  -2.51170188e-04,  -1.16314589e-03,
        -7.00211795e-05,   2.13491847e-03,   4.87995442e-03,
         6.25013639e-03,   5.33888863e-03,   2.72397160e-03,
        -9.37862592e-04,  -4.10850809e-03,  -5.12994025e-03,
        -5.27407551e-03,  -6.74824209e-03,  -8.40904811e-03])

	print "Maxmimum difference between expected values of electric field and computed values: "
	print "Theta direction: %.4e" % (np.nanmax(np.abs(expected_elth-el_th)))
	print "Phi direction: %.4e" % (np.nanmax(np.abs(expected_elph-el_ph)))

	f = pp.figure()
	a1 = f.add_subplot(211)
	a2 = f.add_subplot(212)
	
	a1.plot(lon,el_th,'bo',label='New Just-SAM north-south electric field')
	a1.plot(lon,expected_elth,'r.',label='Original Davitpy SAM north-south electric field')

	a2.plot(lon,el_ph,'bo',label='New Just-SAM east-west electric field')
	a2.plot(lon,expected_elph,'r.',label='Original Davitpy SAM east-west electric field')
	
	a1.legend()
	a2.legend()
	a2.set_xlabel('Longitude')
	pp.show()

def plotSamVel(year,month,day,hour,minute=0,nhours=2,hemi='north'):
	"""
	Plot a vector plot of SAM velocities
	"""
	import datetime,sys
	import numpy as np
	import matplotlib.pyplot as pp
	from geospacepy import satplottools # To make it easier to visualize
	pp.ion()

	hemisign = 1. if hemi is 'north' else -1.
	hemiabrev = 'N' if hemi in ['north','N'] else 'S'

	dt = datetime.datetime(year,month,day,hour,minute,0)
	samList = sam.calcSam(dt,dt+datetime.timedelta(hours=nhours),4,hemi=hemi)
	#sam_imgs = []
	#sam_movie = 'sam_%d_%d_%d_%.2d00_%.2d00.gif' % (year,month,day,hour,hour+np.round(nhours))

	f = pp.figure(figsize=(12,6))
	
	maxpot = 30000.
	norm = mpl.colors.Normalize(vmin=-1*maxpot,vmax=1.*maxpot)

	for iframe,thissam in enumerate(samList):
		#Grid settings
		dlat,dlon,lat0 = 2.,10.,50.
		(flatlat,flatlon,vel_th,vel_ph,verr_th,verr_ph) = thissam.calcFitVel(dlat=dlat,dlon=dlon,lat0=lat0)
		#(lat_grd,lon_grd,el_th,el_ph,eerr_th,eerr_ph)   = samList[0].calcFitEfield(dlat=2,dlon=10,lat0=50)
		(flatlat,flatlon,pot,perr)                      = thissam.calcFitPot(dlat=dlat,dlon=dlon,lat0=lat0)
		obs_lat,obs_lon = np.array(thissam.obs['lat']),np.array(thissam.obs['lon'])
		obs_verr,obs_vlos,obs_azm = np.array(thissam.obs['verr']),np.array(thissam.obs['vlos']),np.array(thissam.obs['azm'])
		obs_vel_th = -1*np.cos(np.radians(obs_azm))*obs_vlos #Equatorward component of line-of-sight directions
		obs_vel_ph = np.sin(np.radians(obs_azm))*obs_vlos #Eastward component of line-of-sight direction
		obs_res = thissam.obs_res.flatten()*1000.
		obs_res_med = np.nanmedian(obs_res)
		obs_res_lq,obs_res_uq = np.nanpercentile(obs_res,25),np.nanpercentile(obs_res,75)

 		#Observation relative error
		obs_rerr = (np.abs(obs_vlos)-obs_verr)/np.abs(obs_vlos)
		obs_rerr[obs_rerr>1]=1.
		obs_rerr[obs_rerr<0]=0.
		
		#Get magnetic local time
		#This longitude is actually MLT longitude, i.e MLT in degrees. Very strange
		flatmlt = flatlon/360.*24. 
		#I think this one is real magnetic longitude?
		obs_mlt = np.mod(obs_lon+thissam.mltDef,360)/360.*24.

		lats = np.arange(lat0,90.+dlat,dlat)
		lons = np.arange(-180.,180.,dlon)

		f.clf()
		acb = f.add_axes([.05,.05,.03,.8])
		a = f.add_axes([.15,.05,.35,.8])
		a2 = f.add_axes([.5,.05,.35,.8])
		acb2 = f.add_axes([.85,.05,.03,.8])
		
		satplottools.draw_dialplot(a)
		satplottools.draw_dialplot(a2)

		X,Y = satplottools.latlt2cart(flatlat,flatmlt,hemiabrev)
		Xobs,Yobs = satplottools.latlt2cart(obs_lat,obs_mlt,hemiabrev)

		#Calculate arrow directions for vector plot
		phi = np.mod(obs_mlt-6.,24.)/12.*np.pi

		vel_east = obs_vel_ph
		vel_eq = obs_vel_th

		eq_hat_x,eq_hat_y = np.cos(phi),np.sin(phi)
		east_hat_x,east_hat_y = -1.*np.sin(phi),np.cos(phi)
		
		#Convert vectors to cartesian from top-down polar
		obs_vel_x = vel_east*east_hat_x + vel_eq*eq_hat_x
		obs_vel_y = vel_east*east_hat_y + vel_eq*eq_hat_y
		
		#vel_x = -1*vel_x
		#vel_y = -1*vel_y

		#Make them into grids
		lat_grd = np.reshape(flatlat,(len(lons),len(lats)))
		lon_grd = np.reshape(flatlon,(len(lons),len(lats)))
		
		X_grd, Y_grd = np.reshape(X,(len(lons),len(lats))),np.reshape(Y,(len(lons),len(lats)))
		pot_grd,pot_err_grd = np.reshape(pot,(len(lons),len(lats))),np.reshape(perr,(len(lons),len(lats)))


		#Draw the potential
		#maxpot = np.nanmax(np.abs(pot))
		maxerr = np.nanmax(np.abs(perr))

		cmap_pot,cmap_err,cmap_obs = pp.cm.get_cmap('seismic'),pp.cm.get_cmap('jet'),pp.cm.get_cmap('bone')
		mappable = a.contourf(X_grd,Y_grd,pot_grd,30,cmap=cmap_pot,alpha=.7,vmin=-1.*maxpot,vmax=1.*maxpot)
		
		err_mappable = a2.contourf(X_grd,Y_grd,pot_err_grd,30,cmap=cmap_err,alpha=.7,vmin=0.,vmax=maxerr)
		
		cb = f.colorbar(mappable,label='Potential',cax=acb)	
		cb = f.colorbar(err_mappable,label='Analysis Error in Potential',cax=acb2)

		a.scatter(Xobs[obs_rerr>0.],Yobs[obs_rerr>0.],s=5,c='w',marker='o',edgecolor='none',cmap=cmap_obs,alpha=.6)
		a.scatter(Xobs[obs_rerr==0.],Yobs[obs_rerr==0.],s=5,c='r',marker='o',edgecolor='none',cmap=cmap_obs,alpha=.6)

		a2.scatter(Xobs,Yobs,s=10*obs_rerr+1,c='k',marker='o',edgecolor='none',cmap=cmap_obs)
		a2.scatter(Xobs[obs_rerr==0.],Yobs[obs_rerr==0.],s=5,c='r',marker='o',edgecolor='none',cmap=cmap_obs)


		Q1 = a.quiver(Xobs[obs_rerr>0.], Yobs[obs_rerr>0.], obs_vel_x[obs_rerr>0.], obs_vel_y[obs_rerr>0.], color='black', 
			angles='xy', scale_units='xy', scale=1000./10.,alpha=.6, headwidth=2., headlength=1.,width=.0025)
		a.quiverkey(Q1,.75,0.05,1000.,'SuperDARN Line-of-sight V\n 1000 m/s')
		
		f.suptitle('SuperDARN Assimilative Mapping: %s-%s' % (thissam.sTime.strftime('%c'),thissam.eTime.strftime('%c')))

		a.set_title('Potential and Ion Drift Velocity')
		a2.set_title('Analysis Error Covariance')
		f.text(.5,0.02,'Vsw: %.3f, IMF By : %.3f, IMF Bz: %.3f' % (thissam.Vsw,thissam.IMFBy,thissam.IMFBz),ha='center')
		f.text(.5,0.98,'E_los Res [mV/m]: %.3f,%.3f,%.3f' % (obs_res_lq,obs_res_med,obs_res_uq),ha='center')


		pngname = 'just_sam_frame_%.4d.png' % (iframe)
		f.savefig(pngname)

		pp.subplots_adjust(wspace=0.)
		pp.pause(.01)
		pp.show()

