# This code is covered under the DavitPy license (GPL v3), which is included with this software
#
# The SAM code was originally authored by Ellen D. P. Cousins, 2014-08
#
# The SAM code was split from the DavitPy SAM feature-fork here:
#     https://github.com/ecousins/davitpy-edc/tree/feature-sam
#
# I have made only minor modifications to Ellen's work
# primarily to clean up the table location handling and addition of a test script
# my purpose was to increase the modularity of this code so it can get more use.
#
# Liam Kilcommons - University of Colorado, Boulder - Colorado Center for Astrodynamics Research
# Mar, 2016
# (C) 2016 University of Colorado AES-CCAR-SEDA (Space Environment Data Analysis) Group

import os
import glob

os.environ['DISTUTILS_DEBUG'] = "1"

from setuptools import setup, Extension
from setuptools.command import install as _install

setup(name='just_sam',
      version = "0.1.0",
      description = "The SuperDARN Assimilative Mapping procedure",
      #author = "VT SuperDARN Lab and friends",
      #author_email = "ajribeiro86@gmail.com",
      author = "Liam Kilcommons / AMIEPy Project",
      author_email = 'liam.kilcommons@colorado.edu',
      url = "https://bitbucket.org/amienext/just_sam",
      download_url = "https://bitbucket.org/amienext/just_sam",
      long_description = "This is Ellen D. P. Cousins' SAM procedure which is seperate from a specific DavitPy version."+\
            "Reference: Cousins, E. D. P., T. Matsuo, and A. D. Richmond (2013), SuperDARN assimilative mapping, "+\
            "J. Geophys. Res. Space Physics, 118, 7954-7962, doi:10.1002/2013JA019321.",
      install_requires=[],
      packages=['just_sam'],
      package_dir={'just_sam' : 'just_sam'},
      package_data={'just_sam': ['tables/eofs/*.dat','tables/amie/*.dat','tables/cs10/*.bsph']}, #data names must be list
      license='LICENSE.txt',
      zip_safe = False,
      classifiers = [
            "Development Status :: 4 - Beta",
            "Topic :: Scientific/Engineering",
            "Intended Audience :: Science/Research",
            "Natural Language :: English",
            "Programming Language :: Python"
            ],
      )